package com.chaosimC.chaoscommons.services;

import com.chaosimC.chaoscommons.models.ChaosType;

public interface ChaosMaker extends Initializable {

    boolean createChaos();
    boolean revertChaos();
    ChaosType getChaosType();

}
