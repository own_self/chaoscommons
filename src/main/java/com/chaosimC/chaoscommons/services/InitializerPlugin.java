package com.chaosimC.chaoscommons.services;

public interface InitializerPlugin {

    void initialize(Object config);
}
