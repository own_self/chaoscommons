package com.chaosimC.chaoscommons.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Map;

public class FileReplaceWriter {

    private static final Logger LOGGER = LogManager.getLogger(FileReplaceWriter.class);

    public static void execute(String sourceFilePath, String destinationFilePath, Map<String, String> replacementMap) {
        BufferedReader bufferedReader = null;
        FileWriter fileWriter = null;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            bufferedReader = new BufferedReader(new FileReader(sourceFilePath));
            String line = null;
            while((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            String template = stringBuilder.toString();
            for(Map.Entry<String, String> entry : replacementMap.entrySet()) {
                template = template.replaceFirst("\\$" + entry.getKey(), entry.getValue());
            }

            fileWriter = new FileWriter(destinationFilePath);
            fileWriter.write(template);
            fileWriter.flush();
        } catch (FileNotFoundException e) {
            LOGGER.error("Could not read template file: " + sourceFilePath, e);
        } catch (IOException e) {
            LOGGER.error("Could not read template file due to exception: ", e);
        } finally {
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    LOGGER.warn("Could not close Buffered Reader.");
                }
            }
            if(fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    LOGGER.warn("Could not close File Writer.");
                }
            }
        }
    }

}
