package com.chaosimC.chaoscommons.services;

public interface ProcessRunner {

    int runProcess(String path, int ttl);
    boolean killProcess(int id);

}
