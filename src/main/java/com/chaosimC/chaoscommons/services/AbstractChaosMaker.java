package com.chaosimC.chaoscommons.services;

public abstract class AbstractChaosMaker implements ChaosMaker {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
