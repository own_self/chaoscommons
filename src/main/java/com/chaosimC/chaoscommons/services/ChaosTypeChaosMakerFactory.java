package com.chaosimC.chaoscommons.services;

import com.chaosimC.chaoscommons.models.ChaosType;

public interface ChaosTypeChaosMakerFactory extends Initializable {

    AbstractChaosMaker getChaosTypeChaosMaker(ChaosType chaosType);
    void registerChaosTypeChaosMaker(ChaosType chaosType, AbstractChaosMaker chaosMaker);

}
