package com.chaosimC.chaoscommons.services;

public interface ProcessListener {

    void onProcessTerminate(int id, int statusCode);

}
