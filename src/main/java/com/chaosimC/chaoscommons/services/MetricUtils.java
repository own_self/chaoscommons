package com.chaosimC.chaoscommons.services;

import com.chaosimC.chaoscommons.config.StatsdConfig;
import com.chaosimC.chaoscommons.models.MetricTagValue;

import java.util.ArrayList;
import java.util.List;

public class MetricUtils {

    public static void increment(String component, String counterMetricName, List<MetricTagValue> metricTagValues) {
        StatsdConfig.statsd.increment(counterMetricName, mapTags(component, metricTagValues));
    }

    public static void recordHistogram(String component, String histMetricName, double value, List<MetricTagValue> metricTagValues) {
        StatsdConfig.statsd.recordHistogramValue(histMetricName, value, mapTags(component, metricTagValues));
    }

    private static String[] mapTags(String component, List<MetricTagValue> metricTagValues) {
        if(metricTagValues == null) {
            metricTagValues = new ArrayList<>();
        }
        metricTagValues.add(new MetricTagValue("component", component));
        return metricTagValues.stream().map(metricTagValue
                -> metricTagValue.getTagName() + ":" + metricTagValue.getTagValue()).toArray(String[]::new);
    }

}
