package com.chaosimC.chaoscommons.services.impl;

import com.chaosimC.chaoscommons.services.ProcessRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.*;

@Service
public class ProcessRunnerImpl implements ProcessRunner {

    private static final Logger LOGGER = LogManager.getLogger(ProcessRunnerImpl.class);
    private final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);

    @Override
    public int runProcess(String command, int ttl) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("sh " + command);
        ProcessExecutor processExecutor = new ProcessExecutor(processBuilder, ttl);
        Future<Integer> processStatusFuture = EXECUTOR_SERVICE.submit(processExecutor);
        try {
            return processStatusFuture.get(ttl, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            LOGGER.error(String.format("Could not execute process %s due to exception: ", command), e);
        }
        return -1;
    }

    @Override
    public boolean killProcess(int id) {
        return false;
    }

    public static class ProcessExecutor implements Callable<Integer> {

        private final ProcessBuilder processBuilder;
        private final int ttl;

        public ProcessExecutor(ProcessBuilder processBuilder, int ttl) {
            this.processBuilder = processBuilder;
            this.ttl = ttl;
        }

        @Override
        public Integer call() throws Exception {
            Process process = processBuilder.start();
            boolean waitResult = process.waitFor(ttl, TimeUnit.SECONDS);
            if (waitResult) {
                return process.exitValue();
            }
            return -1;


//            Process process = Runtime.getRuntime().exec("sh " + shFile);
//            InputStream errorStream = process.getErrorStream();
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(errorStream));
//            String line;
//            while((line = bufferedReader.readLine()) != null) {
//                LOGGER.info("line: " + line);
//            }

        }
    }

}
