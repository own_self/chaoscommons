package com.chaosimC.chaoscommons.services.impl;

import com.chaosimC.chaoscommons.models.ChaosType;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChaosTypeFactory {

    private Map<String, ChaosType> chaosTypeMap;

    public ChaosTypeFactory() {
        chaosTypeMap = new HashMap<>();
    }

    public void registerChaosType(String chaosTypeId, ChaosType chaosType) {
        chaosTypeMap.put(chaosTypeId, chaosType);
    }

    public ChaosType getChaosType(String chaosTypeId) {
        return chaosTypeMap.get(chaosTypeId);
    }

}
