package com.chaosimC.chaoscommons.services.impl;

import com.chaosimC.chaoscommons.services.ProcessListener;

public abstract class AbstractProcessListener implements ProcessListener {

    private ProcessListener processListener;

    public ProcessListener getProcessListener() {
        return processListener;
    }

    public void setProcessListener(ProcessListener processListener) {
        this.processListener = processListener;
    }
}
