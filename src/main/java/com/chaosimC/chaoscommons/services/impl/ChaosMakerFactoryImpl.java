package com.chaosimC.chaoscommons.services.impl;

import com.chaosimC.chaoscommons.models.ChaosType;
import com.chaosimC.chaoscommons.services.AbstractChaosMaker;
import com.chaosimC.chaoscommons.services.ChaosMaker;
import com.chaosimC.chaoscommons.services.ChaosTypeChaosMakerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChaosMakerFactoryImpl implements ChaosTypeChaosMakerFactory {

    @Autowired
    private ChaosTypeFactory chaosTypeFactory;

    private Map<ChaosType, AbstractChaosMaker> chaosTypeChaosMakerMap;

    public ChaosMakerFactoryImpl() {
        this.chaosTypeChaosMakerMap = new HashMap<>();
    }

    public AbstractChaosMaker getChaosTypeChaosMaker(ChaosType chaosType) {
        return chaosTypeChaosMakerMap.get(chaosType);
    }

    public void registerChaosTypeChaosMaker(ChaosType chaosType, AbstractChaosMaker chaosMaker) {
        chaosTypeChaosMakerMap.put(chaosType, chaosMaker);
        chaosTypeFactory.registerChaosType(chaosType.getId(), chaosType);
    }

    public void initialize() {

    }

}
