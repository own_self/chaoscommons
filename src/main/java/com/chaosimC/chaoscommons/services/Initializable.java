package com.chaosimC.chaoscommons.services;

import com.chaosimC.chaoscommons.exceptions.InitializationFailedException;

public interface Initializable {

    void initialize() throws InitializationFailedException;

}
