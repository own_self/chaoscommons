package com.chaosimC.chaoscommons.exceptions;

public class InitializationFailedException extends RuntimeException {
    public InitializationFailedException(String message) {
        super(message);
    }
}
