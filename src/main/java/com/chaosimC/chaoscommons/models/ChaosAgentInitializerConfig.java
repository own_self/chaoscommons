package com.chaosimC.chaoscommons.models;

public class ChaosAgentInitializerConfig {

    private String lokiDataPath;
    private String host;
    private String privateKey;
    private String user;
    private String chaosAgentDataDirectoryPath;

    public String getLokiDataPath() {
        return lokiDataPath;
    }

    public void setLokiDataPath(String lokiDataPath) {
        this.lokiDataPath = lokiDataPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getChaosAgentDataDirectoryPath() {
        return chaosAgentDataDirectoryPath;
    }

    public void setChaosAgentDataDirectoryPath(String chaosAgentDataDirectoryPath) {
        this.chaosAgentDataDirectoryPath = chaosAgentDataDirectoryPath;
    }

    @Override
    public String toString() {
        return "ChaosAgentInitializerConfig{" +
                "host='" + host + '\'' +
                ", privateKey='" + privateKey + '\'' +
                ", user='" + user + '\'' +
                ", chaosAgentDataDirectoryPath='" + chaosAgentDataDirectoryPath + '\'' +
                '}';
    }
}
