package com.chaosimC.chaoscommons.models;

public class PrometheusInitializerConfig {

    private String lokiDataPath;
    private String jmeterHost;
    private String telegrafHosts;
    private String prometheusPath;
    private String updateFilesPath;
    private String prometheusHost;
    private String prometheusHostPrivateKey;
    private String prometheusHostUser;
    private String prometheusHostDirectoryPath;

    private PrometheusConfigStage prometheusConfigStage;

    public String getLokiDataPath() {
        return lokiDataPath;
    }

    public void setLokiDataPath(String lokiDataPath) {
        this.lokiDataPath = lokiDataPath;
    }

    public String getJmeterHost() {
        return jmeterHost;
    }

    public void setJmeterHost(String jmeterHost) {
        this.jmeterHost = jmeterHost;
    }

    public String getTelegrafHosts() {
        return telegrafHosts;
    }

    public void setTelegrafHosts(String telegrafHosts) {
        this.telegrafHosts = telegrafHosts;
    }

    public String getPrometheusPath() {
        return prometheusPath;
    }

    public void setPrometheusPath(String prometheusPath) {
        this.prometheusPath = prometheusPath;
    }

    public String getUpdateFilesPath() {
        return updateFilesPath;
    }

    public void setUpdateFilesPath(String updateFilesPath) {
        this.updateFilesPath = updateFilesPath;
    }

    public PrometheusConfigStage getPrometheusConfigStage() {
        return prometheusConfigStage;
    }

    public void setPrometheusConfigStage(PrometheusConfigStage prometheusConfigStage) {
        this.prometheusConfigStage = prometheusConfigStage;
    }

    public String getPrometheusHost() {
        return prometheusHost;
    }

    public void setPrometheusHost(String prometheusHost) {
        this.prometheusHost = prometheusHost;
    }

    public String getPrometheusHostPrivateKey() {
        return prometheusHostPrivateKey;
    }

    public void setPrometheusHostPrivateKey(String prometheusHostPrivateKey) {
        this.prometheusHostPrivateKey = prometheusHostPrivateKey;
    }

    public String getPrometheusHostUser() {
        return prometheusHostUser;
    }

    public void setPrometheusHostUser(String prometheusHostUser) {
        this.prometheusHostUser = prometheusHostUser;
    }

    public String getPrometheusHostDirectoryPath() {
        return prometheusHostDirectoryPath;
    }

    public void setPrometheusHostDirectoryPath(String prometheusHostDirectoryPath) {
        this.prometheusHostDirectoryPath = prometheusHostDirectoryPath;
    }
}
