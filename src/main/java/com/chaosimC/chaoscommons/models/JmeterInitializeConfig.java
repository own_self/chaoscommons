package com.chaosimC.chaoscommons.models;

public class JmeterInitializeConfig {

    private String scriptBasePath;
    private String privateKeyFilePath;
    private String destinationUser;
    private String destinationFilePath;
    private String host;
    private String protocol;
    private String url;
    private String httpMethod;
    private int port;
    private String jmxTemplateFilePath;
    private String finalJmxFile;
    private int constantThroughputPerMinute;


    public String getScriptBasePath() {
        return scriptBasePath;
    }

    public void setScriptBasePath(String scriptBasePath) {
        this.scriptBasePath = scriptBasePath;
    }

    public String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }

    public void setPrivateKeyFilePath(String privateKeyFilePath) {
        this.privateKeyFilePath = privateKeyFilePath;
    }

    public String getDestinationUser() {
        return destinationUser;
    }

    public void setDestinationUser(String destinationUser) {
        this.destinationUser = destinationUser;
    }

    public String getDestinationFilePath() {
        return destinationFilePath;
    }

    public void setDestinationFilePath(String destinationFilePath) {
        this.destinationFilePath = destinationFilePath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getJmxTemplateFilePath() {
        return jmxTemplateFilePath;
    }

    public void setJmxTemplateFilePath(String jmxTemplateFilePath) {
        this.jmxTemplateFilePath = jmxTemplateFilePath;
    }

    public String getFinalJmxFile() {
        return finalJmxFile;
    }

    public void setFinalJmxFile(String finalJmxFile) {
        this.finalJmxFile = finalJmxFile;
    }

    public int getConstantThroughputPerMinute() {
        return constantThroughputPerMinute;
    }

    public void setConstantThroughputPerMinute(int constantThroughputPerMinute) {
        this.constantThroughputPerMinute = constantThroughputPerMinute;
    }
}
