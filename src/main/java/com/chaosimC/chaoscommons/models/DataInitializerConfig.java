package com.chaosimC.chaoscommons.models;

public class DataInitializerConfig {

    private String dataPath;
    private String lokiPath;
    private String destinationDataPath;
    private String host;
    private String privateKeyPath;
    private String user;
    private boolean isCopyData;

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public void setDestinationDataPath(String destinationDataPath) {
        this.destinationDataPath = destinationDataPath;
    }

    public String getDataPath() {
        return dataPath;
    }

    public String getLokiPath() {
        return lokiPath;
    }

    public void setLokiPath(String lokiPath) {
        this.lokiPath = lokiPath;
    }

    public String getDestinationDataPath() {
        return destinationDataPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPrivateKeyPath() {
        return privateKeyPath;
    }

    public void setPrivateKeyPath(String privateKeyPath) {
        this.privateKeyPath = privateKeyPath;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isCopyData() {
        return isCopyData;
    }

    public void setCopyData(boolean copyData) {
        isCopyData = copyData;
    }

    @Override
    public String toString() {
        return "DataInitializerConfig{" +
                "dataPath='" + dataPath + '\'' +
                ", destinationDataPath='" + destinationDataPath + '\'' +
                ", host='" + host + '\'' +
                ", privateKeyPath='" + privateKeyPath + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
