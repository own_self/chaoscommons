package com.chaosimC.chaoscommons.models;

public class GrafanaInitializerConfig {

    private String lokiDataPath;
    private String scriptsPath;
    private String grafanaFilePath;
    private String grafanaHost;
    private String grafanaHostUser;
    private String privateKey;
    private String grafanaHostScriptPath;
    private String grafanaHostDataPath;
    private GrafanaConfigStage grafanaConfigStage;

    public String getLokiDataPath() {
        return lokiDataPath;
    }

    public void setLokiDataPath(String lokiDataPath) {
        this.lokiDataPath = lokiDataPath;
    }

    public String getScriptsPath() {
        return scriptsPath;
    }

    public void setScriptsPath(String scriptsPath) {
        this.scriptsPath = scriptsPath;
    }

    public String getGrafanaFilePath() {
        return grafanaFilePath;
    }

    public void setGrafanaFilePath(String grafanaFilePath) {
        this.grafanaFilePath = grafanaFilePath;
    }

    public String getGrafanaHost() {
        return grafanaHost;
    }

    public void setGrafanaHost(String grafanaHost) {
        this.grafanaHost = grafanaHost;
    }

    public String getGrafanaHostUser() {
        return grafanaHostUser;
    }

    public void setGrafanaHostUser(String grafanaHostUser) {
        this.grafanaHostUser = grafanaHostUser;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getGrafanaHostScriptPath() {
        return grafanaHostScriptPath;
    }

    public void setGrafanaHostScriptPath(String grafanaHostScriptPath) {
        this.grafanaHostScriptPath = grafanaHostScriptPath;
    }

    public String getGrafanaHostDataPath() {
        return grafanaHostDataPath;
    }

    public void setGrafanaHostDataPath(String grafanaHostDataPath) {
        this.grafanaHostDataPath = grafanaHostDataPath;
    }

    public GrafanaConfigStage getGrafanaConfigStage() {
        return grafanaConfigStage;
    }

    public void setGrafanaConfigStage(GrafanaConfigStage grafanaConfigStage) {
        this.grafanaConfigStage = grafanaConfigStage;
    }
}
