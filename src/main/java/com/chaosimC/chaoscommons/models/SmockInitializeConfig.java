package com.chaosimC.chaoscommons.models;

public class SmockInitializeConfig {

    private String lokiDataPath;
    private String host;
    private String user;
    private String privateKeyFilePath;
    private String jarFilePath;
    private String dataDirectoryPath;

    public String getLokiDataPath() {
        return lokiDataPath;
    }

    public void setLokiDataPath(String lokiDataPath) {
        this.lokiDataPath = lokiDataPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }

    public void setPrivateKeyFilePath(String privateKeyFilePath) {
        this.privateKeyFilePath = privateKeyFilePath;
    }

    public String getJarFilePath() {
        return jarFilePath;
    }

    public void setJarFilePath(String jarFilePath) {
        this.jarFilePath = jarFilePath;
    }

    public String getDataDirectoryPath() {
        return dataDirectoryPath;
    }

    public void setDataDirectoryPath(String dataDirectoryPath) {
        this.dataDirectoryPath = dataDirectoryPath;
    }

    @Override
    public String toString() {
        return "SmockInitializeConfig{" +
                "host='" + host + '\'' +
                ", user='" + user + '\'' +
                ", privateKeyFilePath='" + privateKeyFilePath + '\'' +
                ", jarFilePath='" + jarFilePath + '\'' +
                ", dataDirectoryPath='" + dataDirectoryPath + '\'' +
                '}';
    }
}
